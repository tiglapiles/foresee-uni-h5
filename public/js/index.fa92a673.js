(function(e) {
  function n(n) {
    for (
      var o, r, s = n[0], c = n[1], u = n[2], d = 0, g = [];
      d < s.length;
      d++
    )
      (r = s[d]), a[r] && g.push(a[r][0]), (a[r] = 0);
    for (o in c) Object.prototype.hasOwnProperty.call(c, o) && (e[o] = c[o]);
    l && l(n);
    while (g.length) g.shift()();
    return i.push.apply(i, u || []), t();
  }
  function t() {
    for (var e, n = 0; n < i.length; n++) {
      for (var t = i[n], o = !0, r = 1; r < t.length; r++) {
        var c = t[r];
        0 !== a[c] && (o = !1);
      }
      o && (i.splice(n--, 1), (e = s((s.s = t[0]))));
    }
    return e;
  }
  var o = {},
    a = { index: 0 },
    i = [];
  function r(e) {
    return (
      s.p +
      "js/" +
      ({
        "pages-feeds-index": "pages-feeds-index",
        "pages-home-index": "pages-home-index",
        "pages-message-index": "pages-message-index",
        "pages-mine-index": "pages-mine-index"
      }[e] || e) +
      "." +
      {
        "pages-feeds-index": "ca59d8a1",
        "pages-home-index": "7d170d10",
        "pages-message-index": "b5775b9a",
        "pages-mine-index": "323e29c9"
      }[e] +
      ".js"
    );
  }
  function s(n) {
    if (o[n]) return o[n].exports;
    var t = (o[n] = { i: n, l: !1, exports: {} });
    return e[n].call(t.exports, t, t.exports, s), (t.l = !0), t.exports;
  }
  (s.e = function(e) {
    var n = [],
      t = a[e];
    if (0 !== t)
      if (t) n.push(t[2]);
      else {
        var o = new Promise(function(n, o) {
          t = a[e] = [n, o];
        });
        n.push((t[2] = o));
        var i,
          c = document.createElement("script");
        (c.charset = "utf-8"),
          (c.timeout = 120),
          s.nc && c.setAttribute("nonce", s.nc),
          (c.src = r(e)),
          (i = function(n) {
            (c.onerror = c.onload = null), clearTimeout(u);
            var t = a[e];
            if (0 !== t) {
              if (t) {
                var o = n && ("load" === n.type ? "missing" : n.type),
                  i = n && n.target && n.target.src,
                  r = new Error(
                    "Loading chunk " + e + " failed.\n(" + o + ": " + i + ")"
                  );
                (r.type = o), (r.request = i), t[1](r);
              }
              a[e] = void 0;
            }
          });
        var u = setTimeout(function() {
          i({ type: "timeout", target: c });
        }, 12e4);
        (c.onerror = c.onload = i), document.head.appendChild(c);
      }
    return Promise.all(n);
  }),
    (s.m = e),
    (s.c = o),
    (s.d = function(e, n, t) {
      s.o(e, n) || Object.defineProperty(e, n, { enumerable: !0, get: t });
    }),
    (s.r = function(e) {
      "undefined" !== typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(e, "__esModule", { value: !0 });
    }),
    (s.t = function(e, n) {
      if ((1 & n && (e = s(e)), 8 & n)) return e;
      if (4 & n && "object" === typeof e && e && e.__esModule) return e;
      var t = Object.create(null);
      if (
        (s.r(t),
        Object.defineProperty(t, "default", { enumerable: !0, value: e }),
        2 & n && "string" != typeof e)
      )
        for (var o in e)
          s.d(
            t,
            o,
            function(n) {
              return e[n];
            }.bind(null, o)
          );
      return t;
    }),
    (s.n = function(e) {
      var n =
        e && e.__esModule
          ? function() {
              return e["default"];
            }
          : function() {
              return e;
            };
      return s.d(n, "a", n), n;
    }),
    (s.o = function(e, n) {
      return Object.prototype.hasOwnProperty.call(e, n);
    }),
    (s.p = "/"),
    (s.oe = function(e) {
      throw (console.error(e), e);
    });
  var c = (window["webpackJsonp"] = window["webpackJsonp"] || []),
    u = c.push.bind(c);
  (c.push = n), (c = c.slice());
  for (var d = 0; d < c.length; d++) n(c[d]);
  var l = u;
  i.push([0, "chunk-vendors"]), t();
})({
  0: function(e, n, t) {
    e.exports = t("cd49");
  },
  "034f": function(e, n, t) {
    "use strict";
    var o = t("c1ff"),
      a = t.n(o);
    a.a;
  },
  "3dfd": function(e, n, t) {
    "use strict";
    t.r(n);
    var o = t("94cd"),
      a = t("6f68");
    for (var i in a)
      "default" !== i &&
        (function(e) {
          t.d(n, e, function() {
            return a[e];
          });
        })(i);
    t("034f");
    var r = t("2877"),
      s = Object(r["a"])(a["default"], o["a"], o["b"], !1, null, null, null);
    n["default"] = s.exports;
  },
  "400c": function(e, n, t) {
    (n = e.exports = t("2350")(!1)),
      n.push([
        e.i,
        "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n/*每个页面公共css */",
        ""
      ]);
  },
  "6cdc": function(e, n, t) {
    "use strict";
    (function(e) {
      var n = t("4ea4"),
        o = n(t("e143"));
      (e[""] = !0),
        delete e[""],
        (e.__uniConfig = {
          condition: {
            current: 0,
            list: [{ name: "test", path: "pages/component/view/index" }]
          },
          globalStyle: { backgroundColor: "#F8F8F8" },
          tabBar: {
            color: "#7A7E83",
            selectedColor: "#fff",
            borderStyle: "black",
            backgroundColor: "#2c3e50",
            list: [
              {
                pagePath: "pages/home/index",
                iconPath: "image/home-h.png",
                selectedIconPath: "image/home.png",
                text: "主页",
                redDot: !1,
                badge: ""
              },
              {
                pagePath: "pages/feeds/index",
                iconPath: "image/feeds-h.png",
                selectedIconPath: "image/feeds.png",
                text: "推荐",
                redDot: !1,
                badge: ""
              },
              {
                pagePath: "pages/message/index",
                iconPath: "image/message-h.png",
                selectedIconPath: "image/message.png",
                text: "消息",
                redDot: !1,
                badge: ""
              },
              {
                pagePath: "pages/mine/index",
                iconPath: "image/mine-h.png",
                selectedIconPath: "image/mine.png",
                text: "我的",
                redDot: !1,
                badge: ""
              }
            ]
          }
        }),
        (e.__uniConfig.router = { mode: "hash", base: "/" }),
        (e.__uniConfig["async"] = {
          loading: "AsyncLoading",
          error: "AsyncError",
          delay: 200,
          timeout: 3e3
        }),
        (e.__uniConfig.debug = !1),
        (e.__uniConfig.networkTimeout = {
          request: 6e3,
          connectSocket: 6e3,
          uploadFile: 6e3,
          downloadFile: 6e3
        }),
        (e.__uniConfig.sdkConfigs = {}),
        (e.__uniConfig.qqMapKey = "XVXBZ-NDMC4-JOGUS-XGIEE-QVHDZ-AMFV2"),
        (e.__uniConfig.nvue = { "flex-direction": "column" }),
        o.default.component("pages-home-index", function(e) {
          var n = {
            component: t
              .e("pages-home-index")
              .then(
                function() {
                  return e(t("f5b8"));
                }.bind(null, t)
              )
              .catch(t.oe),
            delay: __uniConfig["async"].delay,
            timeout: __uniConfig["async"].timeout
          };
          return (
            __uniConfig["async"]["loading"] &&
              (n.loading = {
                name: "SystemAsyncLoading",
                render: function(e) {
                  return e(__uniConfig["async"]["loading"]);
                }
              }),
            __uniConfig["async"]["error"] &&
              (n.error = {
                name: "SystemAsyncError",
                render: function(e) {
                  return e(__uniConfig["async"]["error"]);
                }
              }),
            n
          );
        }),
        o.default.component("pages-feeds-index", function(e) {
          var n = {
            component: t
              .e("pages-feeds-index")
              .then(
                function() {
                  return e(t("9db5"));
                }.bind(null, t)
              )
              .catch(t.oe),
            delay: __uniConfig["async"].delay,
            timeout: __uniConfig["async"].timeout
          };
          return (
            __uniConfig["async"]["loading"] &&
              (n.loading = {
                name: "SystemAsyncLoading",
                render: function(e) {
                  return e(__uniConfig["async"]["loading"]);
                }
              }),
            __uniConfig["async"]["error"] &&
              (n.error = {
                name: "SystemAsyncError",
                render: function(e) {
                  return e(__uniConfig["async"]["error"]);
                }
              }),
            n
          );
        }),
        o.default.component("pages-message-index", function(e) {
          var n = {
            component: t
              .e("pages-message-index")
              .then(
                function() {
                  return e(t("e83e"));
                }.bind(null, t)
              )
              .catch(t.oe),
            delay: __uniConfig["async"].delay,
            timeout: __uniConfig["async"].timeout
          };
          return (
            __uniConfig["async"]["loading"] &&
              (n.loading = {
                name: "SystemAsyncLoading",
                render: function(e) {
                  return e(__uniConfig["async"]["loading"]);
                }
              }),
            __uniConfig["async"]["error"] &&
              (n.error = {
                name: "SystemAsyncError",
                render: function(e) {
                  return e(__uniConfig["async"]["error"]);
                }
              }),
            n
          );
        }),
        o.default.component("pages-mine-index", function(e) {
          var n = {
            component: t
              .e("pages-mine-index")
              .then(
                function() {
                  return e(t("5719"));
                }.bind(null, t)
              )
              .catch(t.oe),
            delay: __uniConfig["async"].delay,
            timeout: __uniConfig["async"].timeout
          };
          return (
            __uniConfig["async"]["loading"] &&
              (n.loading = {
                name: "SystemAsyncLoading",
                render: function(e) {
                  return e(__uniConfig["async"]["loading"]);
                }
              }),
            __uniConfig["async"]["error"] &&
              (n.error = {
                name: "SystemAsyncError",
                render: function(e) {
                  return e(__uniConfig["async"]["error"]);
                }
              }),
            n
          );
        }),
        (e.__uniRoutes = [
          {
            path: "/",
            alias: "/pages/home/index",
            component: {
              render: function(e) {
                return e(
                  "Page",
                  {
                    props: Object.assign(
                      { isQuit: !0, isEntry: !0, isTabBar: !0, tabBarIndex: 0 },
                      __uniConfig.globalStyle,
                      {
                        navigateStyle: "custom",
                        titleNView: {
                          backgroundColor: "#f39c12",
                          searchInput: {
                            align: "center",
                            backgroundColor: "#fff",
                            barderRadius: "5px",
                            placeholder: "Search products or suppliers"
                          },
                          buttons: [
                            { type: "menu", float: "left", color: "gray" },
                            { type: "share", float: "right", color: "gray" }
                          ]
                        }
                      }
                    )
                  },
                  [e("pages-home-index", { slot: "page" })]
                );
              }
            },
            meta: {
              id: 1,
              name: "pages-home-index",
              isNVue: !1,
              pagePath: "pages/home/index",
              isQuit: !0,
              isEntry: !0,
              isTabBar: !0,
              tabBarIndex: 0,
              windowTop: 44
            }
          },
          {
            path: "/pages/feeds/index",
            component: {
              render: function(e) {
                return e(
                  "Page",
                  {
                    props: Object.assign(
                      { isQuit: !0, isTabBar: !0, tabBarIndex: 1 },
                      __uniConfig.globalStyle,
                      {
                        navigateStyle: "custom",
                        titleNView: {
                          titleText: "Feeds",
                          titleColor: "black",
                          titleOverflow: "ellipsis",
                          backgroundColor: "#F8F8F8",
                          buttons: [{ type: "favorite" }]
                        }
                      }
                    )
                  },
                  [e("pages-feeds-index", { slot: "page" })]
                );
              }
            },
            meta: {
              id: 2,
              name: "pages-feeds-index",
              isNVue: !1,
              pagePath: "pages/feeds/index",
              isQuit: !0,
              isTabBar: !0,
              tabBarIndex: 1,
              windowTop: 44
            }
          },
          {
            path: "/pages/message/index",
            component: {
              render: function(e) {
                return e(
                  "Page",
                  {
                    props: Object.assign(
                      { isQuit: !0, isTabBar: !0, tabBarIndex: 2 },
                      __uniConfig.globalStyle,
                      {
                        navigateStyle: "custom",
                        titleNView: {
                          titleText: "Message",
                          titleColor: "white",
                          backgroundColor: "#f39c12",
                          buttons: [{ type: "menu", float: "left" }]
                        }
                      }
                    )
                  },
                  [e("pages-message-index", { slot: "page" })]
                );
              }
            },
            meta: {
              id: 3,
              name: "pages-message-index",
              isNVue: !1,
              pagePath: "pages/message/index",
              isQuit: !0,
              isTabBar: !0,
              tabBarIndex: 2,
              windowTop: 44
            }
          },
          {
            path: "/pages/mine/index",
            component: {
              render: function(e) {
                return e(
                  "Page",
                  {
                    props: Object.assign(
                      { isQuit: !0, isTabBar: !0, tabBarIndex: 3 },
                      __uniConfig.globalStyle,
                      {
                        navigateStyle: "custom",
                        titleNView: {
                          titleText: "Mine",
                          titleColor: "white",
                          backgroundColor: "#f39c12",
                          buttons: [{ type: "menu", float: "left" }]
                        }
                      }
                    )
                  },
                  [e("pages-mine-index", { slot: "page" })]
                );
              }
            },
            meta: {
              id: 4,
              name: "pages-mine-index",
              isNVue: !1,
              pagePath: "pages/mine/index",
              isQuit: !0,
              isTabBar: !0,
              tabBarIndex: 3,
              windowTop: 44
            }
          },
          {
            path: "/preview-image",
            component: {
              render: function(e) {
                return e("Page", { props: { navigationStyle: "custom" } }, [
                  e("system-preview-image", { slot: "page" })
                ]);
              }
            },
            meta: { name: "preview-image", pagePath: "/preview-image" }
          },
          {
            path: "/choose-location",
            component: {
              render: function(e) {
                return e("Page", { props: { navigationStyle: "custom" } }, [
                  e("system-choose-location", { slot: "page" })
                ]);
              }
            },
            meta: { name: "choose-location", pagePath: "/choose-location" }
          },
          {
            path: "/open-location",
            component: {
              render: function(e) {
                return e("Page", { props: { navigationStyle: "custom" } }, [
                  e("system-open-location", { slot: "page" })
                ]);
              }
            },
            meta: { name: "open-location", pagePath: "/open-location" }
          }
        ]);
    }.call(this, t("c8ba")));
  },
  "6f68": function(e, n, t) {
    "use strict";
    t.r(n);
    var o = t("8aab"),
      a = t.n(o);
    for (var i in o)
      "default" !== i &&
        (function(e) {
          t.d(n, e, function() {
            return o[e];
          });
        })(i);
    n["default"] = a.a;
  },
  "8aab": function(e, n, t) {
    "use strict";
    var o = t("4ea4");
    Object.defineProperty(n, "__esModule", { value: !0 }), (n.default = void 0);
    var a = o(t("e143")),
      i = a.default.extend({
        mpType: "app",
        onLaunch: function() {
          console.log("App Launch");
        },
        onShow: function() {
          console.log("App Show");
        },
        onHide: function() {
          console.log("App Hide");
        }
      });
    n.default = i;
  },
  "94cd": function(e, n, t) {
    "use strict";
    var o = function() {
        var e = this,
          n = e.$createElement,
          t = e._self._c || n;
        return t("App", { attrs: { keepAliveInclude: e.keepAliveInclude } });
      },
      a = [];
    t.d(n, "a", function() {
      return o;
    }),
      t.d(n, "b", function() {
        return a;
      });
  },
  c1ff: function(e, n, t) {
    var o = t("400c");
    "string" === typeof o && (o = [[e.i, o, ""]]),
      o.locals && (e.exports = o.locals);
    var a = t("4f06").default;
    a("9e1bdf56", o, !0, { sourceMap: !1, shadowMode: !1 });
  },
  cd49: function(e, n, t) {
    "use strict";
    var o = t("4ea4");
    t("cadf"), t("551c"), t("f751"), t("097d"), t("6cdc"), t("1c31");
    var a = o(t("e143")),
      i = o(t("3dfd"));
    (a.default.config.productionTip = !1), new i.default().$mount();
  }
});
