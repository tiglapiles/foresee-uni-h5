(window["webpackJsonp"] = window["webpackJsonp"] || []).push([
  ["pages-message-index"],
  {
    "399c": function(t, e, n) {
      "use strict";
      var a = function() {
          var t = this,
            e = t.$createElement,
            n = t._self._c || e;
          return n(
            "v-uni-view",
            { staticClass: "content" },
            [
              n("v-uni-image", {
                staticClass: "logo",
                attrs: { src: "../../logo.png" }
              }),
              n(
                "v-uni-view",
                [
                  n("v-uni-text", { staticClass: "title" }, [
                    t._v(t._s(t.title))
                  ])
                ],
                1
              )
            ],
            1
          );
        },
        i = [];
      n.d(e, "a", function() {
        return a;
      }),
        n.d(e, "b", function() {
          return i;
        });
    },
    4932: function(t, e, n) {
      "use strict";
      n.r(e);
      var a = n("8022"),
        i = n.n(a);
      for (var o in a)
        "default" !== o &&
          (function(t) {
            n.d(e, t, function() {
              return a[t];
            });
          })(o);
      e["default"] = i.a;
    },
    "58e7": function(t, e, n) {
      var a = n("7169");
      "string" === typeof a && (a = [[t.i, a, ""]]),
        a.locals && (t.exports = a.locals);
      var i = n("4f06").default;
      i("caa6b66a", a, !0, { sourceMap: !1, shadowMode: !1 });
    },
    7169: function(t, e, n) {
      (e = t.exports = n("2350")(!1)),
        e.push([
          t.i,
          ".content[data-v-097065de]{text-align:center;height:%?400?%}.logo[data-v-097065de]{height:%?200?%;width:%?200?%;margin-top:%?200?%}.title[data-v-097065de]{font-size:%?36?%;color:#8f8f94}",
          ""
        ]);
    },
    7622: function(t, e, n) {
      "use strict";
      var a = n("58e7"),
        i = n.n(a);
      i.a;
    },
    8022: function(t, e, n) {
      "use strict";
      var a = n("4ea4");
      Object.defineProperty(e, "__esModule", { value: !0 }),
        (e.default = void 0);
      var i = a(n("e143")),
        o = i.default.extend({
          data: function() {
            return { title: "Hello" };
          },
          onLoad: function() {},
          methods: {}
        });
      e.default = o;
    },
    e83e: function(t, e, n) {
      "use strict";
      n.r(e);
      var a = n("399c"),
        i = n("4932");
      for (var o in i)
        "default" !== o &&
          (function(t) {
            n.d(e, t, function() {
              return i[t];
            });
          })(o);
      n("7622");
      var u = n("2877"),
        r = Object(u["a"])(
          i["default"],
          a["a"],
          a["b"],
          !1,
          null,
          "097065de",
          null
        );
      e["default"] = r.exports;
    }
  }
]);
